# Foto Álbum #
Desenvolvido para teste na empresa Citrus Agência

### Detalhes ###

* Estudar a seguinte API: http://jsonplaceholder.typicode.com/
* Desenvolver um Hotsite contendo uma galeria de fotos. Uma lista de álbuns, cada álbum com sua lista de fotos. Usando os métodos da API pra isso.
* Ao clicar num álbum, você deve consultar a API novamente para buscar as fotos desse álbum. As fotos podem ser clicadas e ampliadas. 
* O layout precisa ser responsivo para acessar de um iPhone 5.
* Após o desenvolvimento, você deve nos enviar os arquivos.
* Na documentação da API tem passo a passo como usá-la. Serão necessárias duas consultas: lista de álbuns e lista de fotos.
* Não é preciso gastar muito tempo tentando usar coisas que ainda não saiba. Nossa ideia é avaliar o conhecimento atual do candidato. Porém, quanto maior a demonstração de conhecimento melhor.
* Liste as fotos numa DIV "em 100%" abaixo do respectivo álbum.

### itens utilizados ###
* jQuery v2.2.2/Fancybox
* Bower
* Grunt
* Less
* GIT/Bitbucket

### Verificar ###
Se preferir faça o clone do repositório **git clone git@bitbucket.org:brunoalbano/foto-album.git**